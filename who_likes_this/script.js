function likes(names) {
    let tam = names.length;
    let str="";
  if (tam === 0){
    str = "no one likes this";
  } else if( tam === 1){
      str = names [0] +" likes this";
  } else if( tam === 2) {
      str = names[0]+" and " + names[1] +" like this";
  } else if( tam === 3){
      str = names[0]+", " + names[1] + " and " + names[2] + " like this";
  } else {
      let n = tam-2;
      str = names[0]+ ", "+ names[1] + " and " + n +" others like this";
  }
  return str;
}