function printerError(s) {
    let tam = s.length;
    let erro = 0;
    let res ="";
    for ( let i = 0; i < tam ; i++){
        if (
            s[i] === "n"||
            s[i] === "o"||
            s[i] === "p"||
            s[i] === "q"||
            s[i] === "r"||
            s[i] === "s"||
            s[i] === "t"||
            s[i] === "u"||
            s[i] === "v"||
            s[i] === "w"||
            s[i] === "x"||
            s[i] === "y"||
            s[i] === "z"
        ){
            erro++;
        }
    }
    res = erro+"/"+tam;
    return res;
}